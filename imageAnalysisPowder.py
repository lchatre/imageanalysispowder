# Authors : Lucas Chatre and Kevin Lachin
# date : 07 January 2024
# The code is originally adapted from https://stackoverflow.com/questions/66757199/color-percentage-in-image-for-python-using-opencv
# The code is designed to calculate the colour percentages for a two-colour image and can be applied to a batch of images contained in a folder.
# If this is the first time, install cv by typing "pip install opencv-python" in the console.

# Module to import
import cv2
import numpy as np
import pandas as pd
import os
import sys
import os.path
import matplotlib.pyplot as plt

def gettingHsvLimit(dossier_hsv, fichier_hsv, scalePercent, polygon, sensibility_hsv, extendHSV):
    # Function to obtain the HSV limit (lower and upper values) of an image.
    # Note that H, S and V values ranges from 0 to 255.
    
    # Get the filename of the image.
    imageFilename = dossier_hsv + fichier_hsv
    # Read the image.
    img = cv2.imread(imageFilename)
    # Resize the image using the resize_img function.
    img_resized = resizeImg(img, scalePercent)
    # Crop the image using the croppingPolygon function.
    imgTarget_hsv_limit = croppingPolygon(img_resized, polygon)
    # Convert the image to HSV colour space.
    hsvLimitImage = cv2.cvtColor(imgTarget_hsv_limit, cv2.COLOR_BGR2HSV)
    # Obtain lower H, S, and V values.
    lower_hsv = [np.min(hsvLimitImage[0,:,0][np.nonzero(hsvLimitImage[0,:,0])]), 
                np.min(hsvLimitImage[0,:,1][np.nonzero(hsvLimitImage[0,:,1])]), 
                np.min(hsvLimitImage[0,:,2][np.nonzero(hsvLimitImage[0,:,2])])]
    # Obtain upper H, S, and V values.
    upper_hsv = [max(hsvLimitImage[0,:,0]), 
                max(hsvLimitImage[0,:,1]), 
                max(hsvLimitImage[0,:,2])]
    # If HSV limit is to be extended.
    if extendHSV==True:
        # Subtract the sensitivity, the value cannot be less than 1. The V value is set to 1 (minimum saturation).
        lowerLimit_hsv_sensibilite = max(lower_hsv[0]-sensibility_hsv,1), max(lower_hsv[1]-sensibility_hsv,1), 1
        # Add the sensitivity, the value cannot be higher than 255. The V value is set to 255 (maximum saturation).
        upperLimit_hsv_sensibilite = upper_hsv[0]+sensibility_hsv, upper_hsv[1]+sensibility_hsv, 255
    else:
        # V value is set to 1 (minimum saturation).
        lowerLimit_hsv_sensibilite = max(lower_hsv[0]-sensibility_hsv,1), max(lower_hsv[1]-sensibility_hsv,1), max(lower_hsv[2]-sensibility_hsv,1)
        # V value is set to 255 (maximum saturation).
        upperLimit_hsv_sensibilite = upper_hsv[0]+sensibility_hsv, upper_hsv[1]+sensibility_hsv, upper_hsv[2]+sensibility_hsv
    
    # Change the limit value to uint8 format.
    lowerValuesHSV = np.asarray(lowerLimit_hsv_sensibilite, dtype=np.uint8)
    upperValuesHSV = np.asarray(upperLimit_hsv_sensibilite, dtype=np.uint8)
    
    return lowerValuesHSV, upperValuesHSV

def croppingPolygon(imgToCrop, polygon):
    # Function for cropping the image with polygon points.
	# Adapted from https://discuss.dizzycoding.com/cropping-concave-polygon-from-image-using-opencv-python/
    
	# Crop the image using polygon points.
    polygonImg = cv2.boundingRect(polygon)
    # Get coordinates and dimensions of the image.
    x, y, w, h = polygonImg
    # Copy the image to a new variable.
    croppedPolygon = imgToCrop[y:y+h, x:x+w].copy()
    # Subtract the minimum along the first axis of the polygon value.
    polygon = polygon - polygon.min(axis=0)
    # Create a mask from polygon points.
    maskPolygon = np.zeros(croppedPolygon.shape[:2], np.uint8)
    # Draw a contour around the mask.
    cv2.drawContours(maskPolygon, [polygon], -1, (255, 255, 255), -1, cv2.LINE_AA)
    # Crop the image from the polygon points
    imgCropped = cv2.bitwise_and(croppedPolygon, croppedPolygon, mask=maskPolygon)
    
    return imgCropped

def resizeImg(imgToResize, scalePercent):
    # Function to resize the image with a scale factor.
    
    # Calculate the new width and height by resizing the scale.
    width = int(imgToResize.shape[1] * scalePercent)
    height = int(imgToResize.shape[0] * scalePercent)
    # Get the size of the resized image.
    newSize = (width, height)
    # Resize the image.
    imgResized = cv2.resize(imgToResize, newSize, None, None, None, cv2.INTER_AREA)
        
    return imgResized

# Parameter to extend the HSV boundary of both colours. This should be used when colours are difficult to extract from each other.
# Note that HSV values range from 0 to 255
sensibilityHsvColor2 = 0
sensibilityHsvColor1 = 0
# Activate sensitivity to extend the HSV limit. This should be used when colours are difficult to extract from each other.
extendHsvColor2 = False
extendHsvColor1 = False
# Frames per second of recording (s-1).
fps = 1
# Time between two images (s).
tReel = 1/fps

# File to test the image processing.
fileTest = "image_6.png"
# File to get HSV value of color1.
calibrationColor1 = 'color1.png'
# File to get HSV value of color2.
calibrationColor2 = 'color2.png'

# Folder containing the images to be analysed.
analyseFolder = 'images/'

# Enable the display of the polygon that crops the image.
# Recommended if this is your first time for the image processing
# If CalibPolygon = True, the code stops after seeing the polygon crop
CalibPolygon = False
# Enable visualisation of image processing
# Recommended if this is your first time for the image processing
# if Calib_Color = True, the code stops after seeing the image processing on one image
CalibColor = False
# Filename of the tested image.
imageTest = analyseFolder+fileTest

# Folder containing the HSV calibration images.
calibFolder = 'calibration/'

# Reading the image to test the image processing.
imgInit = cv2.imread(imageTest)
# Get the width and height of the image.
widthInit = imgInit.shape[1]
heightInit = imgInit.shape[0]
# Rescale the image for proper viewing.
scalePercent = 0.4
img = resizeImg(imgInit, scalePercent)
# Obtain the new width and height of the resized image.
width = int(img.shape[1])
height = int(img.shape[0])

# Definition of cropping polygons with geometric points.
# The first value of the point is its horizontal position, while the second is its vertical position.
# It is recommended to use imageJ to visualise the coordinate.

# Polygon points for image processing.
pts1 = [int(54/widthInit*width), int(278/heightInit*height)]
pts2 = [int(256/widthInit*width), int(114/heightInit*height)]
pts3 = [int(846/widthInit*width), int(126/heightInit*height)]
pts4 = [int(912/widthInit*width), int(909/heightInit*height)]
pts5 = [int(148/widthInit*width), int(920/heightInit*height)]
pts6 = [int(29/widthInit*width), int(645/heightInit*height)]

# Polygon points for the image that get the HSV limit of colour 1.
pts1P = [int(300/widthInit*width), int(100/heightInit*height)]
pts2P = [int(700/widthInit*width), int(100/heightInit*height)]
pts3P = [int(700/widthInit*width), int(300/heightInit*height)]
pts4P = [int(300/widthInit*width), int(300/heightInit*height)]

# Polygon points for the image that get the HSV limit of colour 2.
pts1T = [int(300/widthInit*width), int(600/heightInit*height)]
pts2T = [int(700/widthInit*width), int(600/heightInit*height)]
pts3T = [int(700/widthInit*width), int(900/heightInit*height)]
pts4T = [int(300/widthInit*width), int(900/heightInit*height)]

# Group the polygon points into an array.
targetPolygon = np.array([pts1, pts2, pts3, pts4, pts5, pts6])
polygonColor1 = np.array([pts1P, pts2P, pts3P, pts4P])
polygonColor2 = np.array([pts1T, pts2T, pts3T, pts4T])

# Crop the image using the crop polygon function.
imgTarget = croppingPolygon(img, targetPolygon)

if CalibPolygon == True:
    # Display the resized image.
    cv2.imshow("Image resized", img)
    cv2.waitKey(1)
    cv2.imshow("Cropped image", imgTarget)
    cv2.waitKey(0)
    # Stop the code.
    sys.exit()

if CalibColor==True:
    # Get the HSV limit of colour 1 and colour 2 with the getHSVLimit function.
    lowerValuesHSVcolor1, upperValuesHSVcolor1 = gettingHsvLimit(calibFolder, calibrationColor1, scalePercent, polygonColor1, sensibilityHsvColor1, extendHsvColor1)
    lowerValuesHSVcolor2, upperValuesHSVcolor2 = gettingHsvLimit(calibFolder, calibrationColor2, scalePercent, polygonColor2, sensibilityHsvColor2, extendHsvColor2)

    # Convert image to HSV colour space.
    hsvImage = cv2.cvtColor(imgTarget, cv2.COLOR_BGR2HSV)

    # Create an HSV mask of each colour.
    hsvMaskcolor2 = cv2.inRange(hsvImage, lowerValuesHSVcolor2, upperValuesHSVcolor2)
    hsvMaskcolor1 = cv2.inRange(hsvImage, lowerValuesHSVcolor1, upperValuesHSVcolor1)

    # Extract the pixels belonging to the HSV boundary of each colour.
    hsvOutputcolor2 = cv2.bitwise_and(imgTarget, imgTarget, mask=hsvMaskcolor2)
    hsvOutputcolor1 = cv2.bitwise_and(imgTarget, imgTarget, mask=hsvMaskcolor1)
    # Get the width and height of the image
    width, height = imgTarget.shape[1::-1]
    # Display the resized image.
    cv2.imshow("Image resized", img)
    cv2.waitKey(1)
    # Display the images in the HSV colour space.
    cv2.imshow("Image in the HSV colour space", hsvImage)
    cv2.waitKey(1)
    cv2.imshow("HSV color 2", np.hstack([imgTarget, hsvOutputcolor2]))
    cv2.waitKey(1)
    cv2.imshow("HSV color 1", np.hstack([imgTarget, hsvOutputcolor1]))
    cv2.waitKey(0)
    # Calculate the percentage of colour 2 as (pixelColor2)/(pixelColor2+pixelColor1).
    ratioColor2Hsv = cv2.countNonZero(hsvMaskcolor2)/(cv2.countNonZero(hsvMaskcolor2)+cv2.countNonZero(hsvMaskcolor1))
    print("color 2 percentage",ratioColor2Hsv*100)
    # Stop the code.
    sys.exit()
else:
    # Count the number of files in the folder to analyse.
    fileCount = len(next(os.walk(analyseFolder))[2])-1
    # Initialise the vector that will contain the values.
    colorPercentHSVBoucle = np.zeros(fileCount)

    for i in range (1, fileCount+1):
        # Get the name of the image number i.
        filename_i = 'image_'+str(i)+".png"
        loopImage = analyseFolder+filename_i
        # Read the image i.
        loopImageInit = cv2.imread(loopImage)
        # Resize the image using the resizeImg function.
        targetImg = resizeImg(loopImageInit, scalePercent)
        # Crop the image using the croppingPolygon function.
        imgTargetLoop = croppingPolygon(targetImg, targetPolygon)
        # Get the HSV boundary of colour 1 and colour 2 with the gettingHsvLimit function.
        lowerValuesHSVcolor1, upperValuesHSVcolor1 = gettingHsvLimit(calibFolder, calibrationColor1, scalePercent, polygonColor1, sensibilityHsvColor1, extendHsvColor1)
        lowerValuesHSVcolor2, upperValuesHSVcolor2 = gettingHsvLimit(calibFolder, calibrationColor2, scalePercent, polygonColor2, sensibilityHsvColor2, extendHsvColor2)
        # Convert the image to the HSV colour space.
        hsvImageBoucle = cv2.cvtColor(imgTargetLoop, cv2.COLOR_BGR2HSV)
        # Extract the pixels belonging to the HSV boundary of each colour.
        hsvMaskcolor1Boucle = cv2.inRange(hsvImageBoucle, lowerValuesHSVcolor1, upperValuesHSVcolor1)
        hsvMaskcolor2Boucle = cv2.inRange(hsvImageBoucle, lowerValuesHSVcolor2, upperValuesHSVcolor2)
        # Calculate the percentage of colour 2 as (pixelColor2)/(pixelColor2+pixelColor1).
        ratioColor2Hsv = cv2.countNonZero(hsvMaskcolor2Boucle)/(cv2.countNonZero(hsvMaskcolor2Boucle)+cv2.countNonZero(hsvMaskcolor1Boucle))
        # Stores the ratioColor2Hsv value in the colourPercentHSVBoucle vector.
        colorPercentHSVBoucle[i-1]=ratioColor2Hsv*100
        print ('image',i,'            color2 percentage=', "{:10.2f}".format(colorPercentHSVBoucle[i-1]))
    
    # Create a vecteur from 0 to the number of images.
    numImage = np.linspace(0, fileCount, fileCount)

    # Convert the number of image to time if FPS is known (s)
    tReal = numImage/fps
    # Plot colour 2 variation over time normalised
    plt.figure()
    plt.plot(tReal, colorPercentHSVBoucle, '-x', markersize=10) 
    plt.grid(visible=True)
    plt.xlabel("Time since acquistion (s)")
    plt.ylabel("colour 2 percentage (%)")
    plt.ylim(0, 100)


    # Export variable into Excel file

    VariationColor2 = np.column_stack((colorPercentHSVBoucle, numImage, tReal))
    VariationColor2Export = pd.DataFrame(VariationColor2)
    VariationColor2Export = VariationColor2Export.rename(columns={0:'color2 percentage (%)',
                                                                          1:'Image number (-)',
                                                                          2:'Real time (s)'})
    VariationColor2Export.to_excel(r'Variation_colour_2.xlsx', index = False)

